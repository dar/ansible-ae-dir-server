---
# Apply AE-DIR configuration to OpenLDAP's slapd
#-----------------------------------------------------------------------

# basic stuff
#-----------------------------------------------------------------------

- name: "Disable and stop slapd"
  service:
    name: slapd
    state: stopped
    enabled: no
  ignore_errors: yes
  when: aedir_service_manager != "systemd"

- block:
  - name: "Disable and stop slapd.service"
    systemd:
      name: slapd
      state: stopped
      enabled: no
    ignore_errors: yes
  - name: "Mask systemd unit for slapd.service"
    systemd:
      name: slapd
      masked: yes
    ignore_errors: yes
  when: aedir_service_manager == "systemd"

- name: "Delete the package dynamic configuration directory {{ openldap_path.conf_prefix }}/slapd.d"
  file:
    path: "{{ openldap_path.conf_prefix }}/slapd.d"
    state: absent

- name: "Replace static config file {{ openldap_path.conf_prefix }}/slapd.conf"
  copy:
    content: "# This slapd.conf was deliberately made non-functional"
    dest: "{{ openldap_path.conf_prefix }}/slapd.conf"
    owner: root
    group: root
    mode: 0o0600

- name: "Create OpenLDAP config directories"
  file:
    path: "{{ item }}"
    state: directory
    owner: root
    group: root
    mode: 0o0755
  with_items:
    - "{{ openldap_slapd_conf|dirname }}"
    - "{{ aedir_schema_prefix }}"
    - "{{ aedir_rundir }}"
  notify:
    - "restart {{ openldap_service_name }}"

# database directory structure
#-----------------------------------------------------------------------

- name: "Create OpenLDAP run directory {{ openldap_rundir }}"
  file:
    path: "{{ openldap_rundir }}"
    state: directory
    owner: root
    group: "{{ openldap_slapd_group }}"
    mode: 0o0775
  notify:
    - "restart {{ openldap_service_name }}"

- name: "Create OpenLDAP database root directory"
  file:
    path: "{{ openldap_data }}"
    state: directory
    owner: "{{ openldap_slapd_user }}"
    group: "{{ openldap_slapd_group }}"
    mode: 0o0750
  notify:
    - "restart {{ openldap_service_name }}"

- name: "Create database directories"
  file:
    path: "{{ openldap_data }}/{{ item }}"
    state: directory
    owner: "{{ openldap_slapd_user }}"
    group: "{{ openldap_slapd_group }}"
    mode: 0o0750
  with_items:
    - accesslog
    - um
  notify:
    - "restart {{ openldap_service_name }}"

- name: "Create session database directory on {{ openldap_role }}"
  file:
    path: "{{ openldap_data }}/session"
    state: directory
    owner: "{{ openldap_slapd_user }}"
    group: "{{ openldap_slapd_group }}"
    mode: 0o0750
  notify:
    - "restart {{ openldap_service_name }}"
  when: openldap_role == 'provider'

- name: "Fix ownership database directories"
  file:
    path: "{{ openldap_data }}/{{ item }}"
    owner: "{{ openldap_slapd_user }}"
    group: "{{ openldap_slapd_group }}"
    recurse: yes
  with_items:
    - accesslog
    - um
  notify:
    - "restart {{ openldap_service_name }}"

- name: "Fix ownership session database directory on {{ openldap_role }}"
  file:
    path: "{{ openldap_data }}/session"
    state: directory
    owner: "{{ openldap_slapd_user }}"
    group: "{{ openldap_slapd_group }}"
    recurse: yes
  notify:
    - "restart {{ openldap_service_name }}"
  when: openldap_role == 'provider'

# slapd.conf and schema files
#-----------------------------------------------------------------------

- name: "Install various OpenLDAP schema files"
  copy:
    src: "schema/{{ item }}"
    dest: "{{ aedir_schema_prefix }}/{{ item }}"
    owner: root
    group: "{{ openldap_slapd_group }}"
    mode: 0o0644
  with_items: "{{ openldap_schema_files }}"
  notify:
    - "restart {{ openldap_service_name }}"

- name: "Create rootDSE.ldif"
  template:
    src: "slapd/rootDSE.ldif.j2"
    dest: "{{ openldap_slapd_conf|dirname }}/rootDSE.ldif"
    owner: root
    group: "{{ openldap_slapd_group }}"
    mode: 0o0640
  notify:
    - "restart {{ openldap_service_name }}"

- name: "Create /etc/sasl2/slapd.conf on {{ lsb_id }}"
  template:
    src: "slapd/sasl2.conf.j2"
    dest: "/etc/sasl2/slapd.conf"
    owner: root
    group: "{{ openldap_slapd_group }}"
    mode: 0o0640
  notify:
    - "restart {{ openldap_service_name }}"
  when: lsb_id != "Debian"

- name: "Create role-specific slapd.conf"
  template:
    src: "slapd/{{ openldap_role }}.conf.j2"
    dest: "{{ openldap_slapd_conf }}"
    owner: root
    group: "{{ openldap_slapd_group }}"
    mode: 0o0640
    validate: "{{ openldap_path.slapd_exec }} -T test -u -f %s -d config"
  notify:
    - "restart {{ openldap_service_name }}"

- name: "Install systemd unit file for {{ openldap_service_name }}"
  template:
    src: "systemd/{{ openldap_service_name }}.service.j2"
    dest: "{{ aedir_systemd_dir }}/{{ openldap_service_name }}.service"
    owner: root
    group: root
    mode: 0o0644
  notify:
    - "restart {{ openldap_service_name }}"
  when: aedir_service_manager == "systemd"

- name: "Create client config file {{ openldap_path.conf_prefix }}/ldap.conf from template"
  template:
    src: "ldap.conf.j2"
    dest: "{{ openldap_path.conf_prefix }}/ldap.conf"
    owner: root
    group: root
    mode: 0o0644
  notify:
    - "restart {{ openldap_service_name }}"

- name: "Create AppArmor profile {{ openldap_service_name }}"
  template:
    src: "apparmor/{{ openldap_service_name }}.j2"
    dest: "{{ apparmor_profiles_dir }}/{{ openldap_service_name }}"
    owner: root
    group: root
    mode: 0o0644
  notify:
    - "restart apparmor"
    - "restart {{ openldap_service_name }}"
  when: apparmor_enabled == True
